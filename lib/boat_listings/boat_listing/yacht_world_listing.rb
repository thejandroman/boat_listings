# frozen_string_literal: true

require 'boat_listings/boat_listing/default_listing'

module BoatListings
  module BoatListing
    # Yacht World Boat
    class YachtWorldListing < BoatListings::BoatListing::DefaultListing
      attr_reader :url, :sku, :description, :price

      def initialize(listing)
        @listing = listing
        parse_meta
      end

      def loa
        @loa ||= loa_year[0].strip.to_i
      end

      def year
        @year ||= loa_year[1].strip
      end

      def location
        @location ||= @listing.css('.listing-card-location').text.strip
      end

      private

      def parse_meta
        @listing.css('meta').each do |meta|
          property = meta.attribute('property').value
          next unless property

          instance_variable_set("@#{property}", meta.attribute('content')&.value)
        end
      end

      def loa_year
        @loa_year ||= @listing.css('.listing-card-length-year').text.split('/')
      end
    end
  end
end
