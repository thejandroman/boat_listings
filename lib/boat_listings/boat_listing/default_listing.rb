# frozen_string_literal: true

module BoatListings
  module BoatListing
    class DefaultListing
      def url; end

      def sku; end

      def description; end

      def price; end

      def loa; end

      def year; end

      def location; end
    end
  end
end
