# frozen_string_literal: true

module BoatListings
  class BoatListing
    def url; end

    def sku; end

    def description; end

    def price; end

    def loa; end

    def year; end

    def location; end
  end
end
