# frozen_string_literal: true

require 'HTTParty'
require 'nokogiri'

require 'boat_listings/listing_page/default_page'
require 'boat_listings/boat_listing/yacht_world_listing'

module BoatListings
  module ListingPage
    # Yacht World Listings
    class YachtWorldPage < BoatListings::ListingPage::DefaultPage
      DEFAULT_PARAMS = { length: '35-50', price: '0-50000' }.freeze

      def initialize(query_params = {})
        @base_uri = 'https://www.yachtworld.com/boats-for-sale/condition-used/type-sail/region-northamerica/sort-price:asc/'
        @query_params = DEFAULT_PARAMS.merge(query_params)
      end

      def page_html
        @page_html ||= HTTParty.get(@base_uri, query: @query_params).body
      end

      def html_doc
        @html_doc ||= Nokogiri::HTML(page_html)
      end

      def boat_listings
        @boat_listings ||= begin
                             listings = html_doc.css('.listings-container').css('a')
                             listings.map do |listing|
                               BoatListings::BoatListing::YachtWorldListing.new(listing)
                             end
                           end
      end
    end
  end
end
