# frozen_string_literal: true

module BoatListings
  module ListingPage
    class DefaultPage
      def boat_listings
        []
      end
    end
  end
end
