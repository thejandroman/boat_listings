# frozen_string_literal: true

require 'boat_listings/version'
require 'boat_listings/listing_page/yacht_world_page'
require 'boat_listings/boat_listing/yacht_world_listing'

module BoatListings
  class Error < StandardError; end
  # Your code goes here...
end
