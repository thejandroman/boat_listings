#!/usr/bin/env ruby
# frozen_string_literal: true

require 'google_drive'
require 'pry'
require 'boat_listings'

SPREADSHEET_ID = '1Opp80mA28vcAnl24zbKlRTXlLuVt-DFIrKyCKYPnHJk'
CURRENT_TIME   = DateTime.now.strftime('%m/%d/%Y %H:%M:%S')

i = 0
boat_listings = []

loop do
  yw = BoatListings::ListingPage::YachtWorldPage.new(page: i)
  puts "page: #{i}"
  boats = yw.boat_listings.select(&:price)

  break if boats.count.zero?

  boat_listings.push(boats)
  i += 1
end

# yw = BoatListings::ListingPage::YachtWorldPage.new(page: 54)
# boats = yw.boat_listings.select(&:price)
# boat_listings.push(boats)

# binding.pry
# exit

session = GoogleDrive::Session.from_config('config.json')
sh = SPREADSHEET_ID
ws = session.spreadsheet_by_key(sh).worksheets[0]

color_cells = []
boat_listings.flatten.each do |boat|
  boat_price = boat.price.to_f
  next if boat_price > 50_000

  found_index = ws.rows.find_index { |cols| cols[0] == boat.sku }
  boat_array = [boat.sku, boat.description, boat.price, boat.loa, boat.year, boat.location, boat.url, CURRENT_TIME]

  if found_index.nil?
    cell_data = { row: ws.num_rows + 1, col_length: boat_array.length, color: GoogleDrive::Worksheet::Colors::GREEN }
    ws.insert_rows(cell_data[:row], [boat_array])
    color_cells.push(cell_data)
    ws.synchronize
    next
  end

  sheet_price = ws[found_index + 1, 3].tr('$,', '').to_f
  percentage  = sheet_price * 0.05
  next if boat_price.between?(sheet_price - percentage, sheet_price + percentage)

  cell_data = { row: found_index + 1, col_length: boat_array.length, color: GoogleDrive::Worksheet::Colors::YELLOW }
  ws.update_cells(cell_data[:row], 1, [boat_array])
  color_cells.push(cell_data)
end

color_cells.each do |cell_data|
  ws.set_background_color(cell_data[:row], 1, 1, cell_data[:col_length], cell_data[:color])
end

ws.save
